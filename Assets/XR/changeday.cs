using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeday : MonoBehaviour
{
    public GameObject batton;
    public GameObject Sun;
    bool x = true;

    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == batton.name)
        {
           x = !x;
            change();
        }
    }
    public void change()
    {
        if (x == false)
        {
            Sun.SetActive(false);
        }
        if (x)
        {
            Sun.SetActive(true);
        }
    }
}
