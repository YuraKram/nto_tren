using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openkriska : MonoBehaviour
{
    public GameObject krishka;
    public GameObject zamok;
    public GameObject player;
    bool y = false;
    Animator animatorkrishka;
    Animator animatorzamok;
    // Start is called before the first frame update
   
    void Start()
    {
        animatorkrishka = krishka.GetComponent<Animator>();
    }

    public void Open()
    {
        animatorkrishka.SetBool("open", y);
    }
    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        bool x = openzamok.x;
        if (other.gameObject.name == player.name & x == true)
        {
            Debug.Log(x) ;
            y = !y;
            Open();
        }
    }
}
