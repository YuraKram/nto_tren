﻿using UnityEngine;
using System.Collections;

public class TriggerAnimatorControl : MonoBehaviour {
	Animator animatorleft;
	Animator animatorright;
	public GameObject right;
	public GameObject left;
	public GameObject player;
void Start()
	{
		animatorleft = left.GetComponent<Animator>();
		animatorright = right.GetComponent<Animator>();
	}
	public void Open()
	{
		animatorleft.SetBool("inzone", true);
		animatorright.SetBool("inzone", true);
	}
	public void Close()
	{
		animatorleft.SetBool("inzone", false);
		animatorright.SetBool("inzone", false);
	}
        void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.name == player.name) {
			Open();
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.name == player.name) {
			Close();
		}
	}
}
