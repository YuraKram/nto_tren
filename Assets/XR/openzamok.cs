using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openzamok : MonoBehaviour
{
    public GameObject zamok;
    public static bool x = false;
    public GameObject player;
    Animator animatorzamok;
    // Start is called before the first frame update
    void Start()
    {
        animatorzamok = zamok.GetComponent<Animator>();
    }

    public void Open()
    {
        animatorzamok.SetBool("open", x);
    }
    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == player.name)
        {
            x = !x;
            Open();
        }
    }
}
