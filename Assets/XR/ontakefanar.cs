using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ontakefanar : MonoBehaviour
{
    public GameObject player;
    public GameObject fonaronfloor;
    public GameObject fonarinhead;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == player.name)
        {
            fonaronfloor.SetActive(false);
            fonarinhead.SetActive(true);
        }
    }
}
